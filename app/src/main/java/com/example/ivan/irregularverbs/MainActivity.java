package com.example.ivan.irregularverbs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    Vector dictionary;
    boolean isFirstSide;
    int currentNumberOfSet;
    Words currentWord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currentNumberOfSet = 1;
        InitiateDictionary();
        isFirstSide = true;

        currentWord = (Words) dictionary.get(currentNumberOfSet);

        TextView myTextView = findViewById(R.id.mytext);
        myTextView.setText(currentWord.Russian);

        TextView pageView = findViewById(R.id.page);
        pageView.setText(currentNumberOfSet + " из " + dictionary.size());
    }

    public void changeWord(android.view.View view) {
        TextView myTextView = (TextView) findViewById(R.id.mytext);
        Words setOfVerbs = (Words) dictionary.get(currentNumberOfSet);
        if (isFirstSide) {
            myTextView.setText(setOfVerbs.Russian +
                    "\n" + setOfVerbs.BaseForm +
                    "\n" + setOfVerbs.PastSimple +
                    "\n" + setOfVerbs.PastPaticiple);
            if (currentNumberOfSet >= dictionary.size() - 1)
                currentNumberOfSet = 1;
            else
                currentNumberOfSet++;
        } else {
            TextView pageView = findViewById(R.id.page);
            pageView.setText(currentNumberOfSet + " из " + dictionary.size());
            myTextView.setText(setOfVerbs.Russian);
        }
        isFirstSide = !isFirstSide;
    }

    private void InitiateDictionary() {
        dictionary = new Vector();
        InputStream inputStream = getResources().openRawResource(R.raw.dictionary);
        InputStreamReader inputreader = new InputStreamReader(inputStream);
        CSVReader reader = new CSVReader(inputreader);
        String [] nextLineOfVerbs;
        try {
            while ((nextLineOfVerbs = reader.readNext()) != null) {
                Words word = new Words();
                word.Russian = nextLineOfVerbs[0];
                word.BaseForm = nextLineOfVerbs[1];
                word.PastSimple = nextLineOfVerbs[2];
                word.PastPaticiple = nextLineOfVerbs[3];
                dictionary.add(word);
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
